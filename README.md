# SSMcSecretFace
SSMcSecretFace is a client used for retrieving SSM Parameter Store parameters from the AWS [Parameter Store Lambda Extension](https://docs.aws.amazon.com/systems-manager/latest/userguide/ps-integration-lambda-extensions.html).  
If the Parameter Store Lambda extension is _not_ being used this library will do nothing for you.  

Contrary to the name, this module does not support requests for AWS Secrets Manager secrets. 

### Usage
#### Minimal
To use the default values simply create a client and call `GetParameter`:
```go
import ssm "gitlab.com/naqll/ssmcsecretface"

client, err := ssm.NewSSMParameterClient()
...
parameter, err := client.GetParameter("my-parameter", nil)
```

#### Selectors
An optional map of selectors can be provided to the `GetParameter` method per request.  
These include:
  - `label` - The parameter label
  - `version` - The specific parameter version to retrieve
  - `withDecryption` - Whether the parameter should be decrypted

For more details, see: https://docs.aws.amazon.com/systems-manager/latest/userguide/ps-integration-lambda-extensions.html#sample-commands-ps

```go
import ssm "gitlab.com/naqll/ssmcsecretface"

client, err := ssm.NewSSMParameterClient()
...
selectors := ssm.ParameterSelectors{
    "withDecryption": "true", 
    "label":          "dev",
}
parameter, err := client.GetParameter("my-parameter", selectors)
```

#### Options
If you've modified the `PARAMETERS_SECRETS_EXTENSION_HTTP_PORT` environment variable or wish to enable certain selectors by default, you can use the following options:
  - `WithPort(UINT16)`
    - Default: `2773` 
    - Changes the port used for requests to the Lambda extension
      - Note: This must match the `PARAMETERS_SECRETS_EXTENSION_HTTP_PORT` environment variable
  - `WithDecryption(BOOL)`
    - Default: `false` 
    - Changes the default `withDecryption` selector to `true` 
  - `WithTimeout(INT)`
    - Default: `5`
    - Changes the HTTP client timeout seconds for requests made to the Lambda extension layer
  - `WithVersion(INT)`
    - Default: Not set; SSM returns latest available version by default
    - Sets the default parameter version number to use for each request
  - `WithLabel(STRING)`
    - Default: Not set
    - Sets the default parameter label to use for reach request

These options may still be overridden by the use of selectors on individual requests.

```go
import ssm "gitlab.com/naqll/ssmcsecretface"

client, err := ssm.NewSSMParameterClient(
    ssm.WithTimeout(2),
    ssm.WithPort(8080),
    ssm.WithVersion(12),
    ssm.WithDecryption(true),
)

// This parameter will be decrypted per the above option
decryptedParam, err := client.GetParameter("my-parameter", nil)
...


selectors := ssm.ParameterSelectors{
    "withDecryption": "false",
}
// This parameter will NOT be decrypted as the above selector overrides the default option
parameter, err := client.GetParameter("my-parameter", selectors)
```
