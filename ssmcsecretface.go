package ssmcsecretface

import (
	"errors"
	"net/http"
	"time"
)

// SSMParameterClient is used to retrieve SSM parameters from the local path provided by the
// SSM Parameter Store Lambda extension
type SSMParameterClient interface {
	GetParameter(name string, selectors ParameterSelectors) (*Parameter, error)
}

// An Option allows changing default values for parameter requests
type Option func(options *options) error

type options struct {
	port           *uint16
	timeout        *int
	version        *int
	label          *string
	withDecryption *bool
}

// ssmClient implements the SSMParameterClient interface
type ssmClient struct {
	*http.Client

	port           uint16
	timeout        int
	withDecryption bool
	version        *int
	label          *string
}

// NewSSMParameterClient returns a new SSMParameterClient with any optional options applied
func NewSSMParameterClient(opts ...Option) (SSMParameterClient, error) {
	var setOptions options
	for _, opt := range opts {
		err := opt(&setOptions)
		if err != nil {
			return nil, err
		}
	}

	client := &ssmClient{
		Client: &http.Client{Timeout: 5 * time.Second},
	}

	if setOptions.port != nil {
		client.port = *setOptions.port
	} else {
		client.port = defaultParameterGETPort
	}

	if setOptions.timeout != nil {
		client.Client.Timeout = time.Duration(*setOptions.timeout) * time.Second
	}
	if setOptions.withDecryption != nil {
		client.withDecryption = *setOptions.withDecryption
	}
	if setOptions.version != nil {
		client.version = setOptions.version
	}
	if setOptions.label != nil {
		client.label = setOptions.label
	}

	return client, nil
}

// WithPort allows specifying a non-default port to use to reach the Lambda extension.
// If you have not changed the default 2773 port via the `PARAMETERS_SECRETS_EXTENSION_HTTP_PORT`
// environment variable then there is no need to specify this option.
func WithPort(port uint16) Option {
	return func(options *options) error {
		if port < 0 || port > 65535 {
			return errors.New("port must be >= 0 and <= 65535")
		}
		options.port = &port
		return nil
	}
}

// WithDecryption allows setting the default "withDecryption" selector for Parameter Store requests.
// The default value is false so this is only needed if you wish for all requests to have this value
// set to true by default.
// Requests can still override this default by specifying a "withDecryption" value via selector.
func WithDecryption(withDecryption bool) Option {
	return func(options *options) error {
		options.withDecryption = &withDecryption
		return nil
	}
}

// WithTimeout sets the HTTP client timeout seconds for requests made to the Lambda extension endpoint.
// Default is 5 seconds.
func WithTimeout(timeout int) Option {
	return func(options *options) error {
		if timeout <= 0 {
			return errors.New("timeout seconds must be >= 0")
		}
		options.timeout = &timeout
		return nil
	}
}

// WithVersion sets the default parameter version number to use for each request.
// Requests can still override this default by specifying a `version` value via selector.
//
// Note: Per AWS, versions and labels are mutually-exclusive:
// "You can also specify either a version or a label, but not both.
// If you do, only the first of these that is placed after question mark (?) in the URL is used."
// https://docs.aws.amazon.com/systems-manager/latest/userguide/ps-integration-lambda-extensions.html#sample-commands-ps
func WithVersion(version int) Option {
	return func(options *options) error {
		if version <= 0 {
			return errors.New("version must be > 0")
		}
		options.version = &version
		return nil
	}
}

// WithLabel sets the default parameter label name to use for each request.
// Requests can still override this default by specifying a `label` value via selector.
//
// Note: Per AWS, versions and labels are mutually-exclusive:
// "You can also specify either a version or a label, but not both.
// If you do, only the first of these that is placed after question mark (?) in the URL is used."
// https://docs.aws.amazon.com/systems-manager/latest/userguide/ps-integration-lambda-extensions.html#sample-commands-ps
func WithLabel(label string) Option {
	return func(options *options) error {
		if label == "" {
			return errors.New("label cannot be empty")
		}
		options.label = &label
		return nil
	}
}
