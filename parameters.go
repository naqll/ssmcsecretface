package ssmcsecretface

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
)

const (
	parameterGETHost        = "http://localhost"
	parameterGETPath        = "systemsmanager/parameters/get"
	defaultParameterGETPort = 2773
)

// Parameter is the SSM parameter details
type Parameter struct {
	Name             string    `json:"Name"`
	ARN              string    `json:"ARN"`
	Type             string    `json:"Type"`
	DataType         string    `json:"DataType"`
	Value            string    `json:"Value"`
	Version          int       `json:"Version"`
	LastModifiedDate time.Time `json:"LastModifiedDate"`
}

// ParameterSelectors is a map of key value pairs for various parameter request options such as
// the parameter version, parameter label, or whether the parameter is encrypted.
// For more details see: https://docs.aws.amazon.com/systems-manager/latest/userguide/ps-integration-lambda-extensions.html#sample-commands-ps
type ParameterSelectors map[string]string

type parameterResult struct {
	Parameter Parameter `json:"Parameter"`
}

// GetParameter retrieves the parameter with the given name or ARN from SSM Parameter Store
func (s ssmClient) GetParameter(name string, selectors ParameterSelectors) (*Parameter, error) {
	urlValues := &url.Values{}
	urlValues.Set("name", name)

	// Add default options provided to the constructor
	if s.version != nil {
		urlValues.Set("version", strconv.Itoa(*s.version))
	}
	if s.label != nil {
		urlValues.Set("label", *s.label)
	}
	urlValues.Set("withDecryption", strconv.FormatBool(s.withDecryption))

	// Add per-request selectors which may override the above defaults
	for k, v := range selectors {
		urlValues.Set(k, v)
	}

	path := fmt.Sprintf("%s:%d/%s?%s", parameterGETHost, s.port, parameterGETPath, urlValues.Encode())
	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		return nil, fmt.Errorf("error creating GET HTTP request: %s", err.Error())
	}
	req.Header.Set("X-Aws-Parameters-Secrets-Token", os.Getenv("AWS_SESSION_TOKEN"))

	resp, err := s.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error retrieving SSM parameter: %s", err.Error())
	}
	defer resp.Body.Close()

	var response parameterResult
	if b, err := io.ReadAll(resp.Body); err != nil {
		return nil, fmt.Errorf("error reading SSM parameter response body: %s", err.Error())
	} else if resp.StatusCode != 200 {
		return nil, fmt.Errorf("received %d status code while retrieving SSM parameter: %s", resp.StatusCode, b)
	} else if err = json.Unmarshal(b, &response); err != nil {
		return nil, fmt.Errorf("error unmarshalling SSM parameter response: %s", err.Error())
	}

	return &response.Parameter, nil
}
